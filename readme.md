Simple helpers to simplify

- dynamically compiling Java source code without storing it in files
- invoking constructors and methods by name with concrete arguments

The tests show some ways to use these helpers.

This stuff is mostly a proof of concept, far from sophisticated. I guess many features could be added, but for simple cases, the code might be helpful.
