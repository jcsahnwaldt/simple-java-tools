package simple.tools;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class SimpleInvoker {

    public final Class<?> cls;

    public SimpleInvoker(Class<?> cls) {
        this.cls = cls;
    }

    public Object newInstance() {
        try {
            Constructor<?> ctor = cls.getDeclaredConstructor();
            ctor.setAccessible(true);
            return ctor.newInstance();
        }
        catch (ReflectiveOperationException e) {
            throw new RuntimeException(e.toString(), e);
        }
    }

    public Object invoke(Object target, String methodName, Object... args) {
        Class<?>[] types = new Class<?>[args.length];
        for (int i = 0; i < args.length; i++) types[i] = args[i].getClass();
        try {
            Method method = cls.getDeclaredMethod(methodName, types);
            method.setAccessible(true);
            return method.invoke(target, args);
        }
        catch (ReflectiveOperationException e) {
            throw new RuntimeException(e.toString(), e);
        }
    }
}
