package simple.tools;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static simple.tools.SimpleCompiler.*;

public class SimpleTests {

    @Test
    public void testSimple() throws Exception {

        String fooJava =
        "package foo; "+
        "class Foo { "+
        "  Foo foo() { return this; } "+
        "}";

        SimpleCompiler compiler = new SimpleCompiler();
        Class<?> fooCls = compiler.compile("foo.Foo", fooJava);
        SimpleInvoker fooInv = new SimpleInvoker(fooCls);
        Object foo1 = fooInv.newInstance();
        Object foo2 = fooInv.invoke(foo1, "foo");
        assertSame(foo1, foo2);
    }

    @Test
    public void testNoRedefine() throws Exception {
        SimpleCompiler compiler = new SimpleCompiler();

        String java1 = "class Foo {}";
        Class<?> cls1 = compiler.compile("Foo", java1);
        assertFalse(Cloneable.class.isAssignableFrom(cls1));

        String java2 = "class Foo implements Cloneable {}";
        Class<?> cls2 = compiler.compile("Foo", java2);
        // class was compiled, but ClassLoader returns old class
        assertFalse(Cloneable.class.isAssignableFrom(cls2));
        assertSame(cls1, cls2);
    }

    @Test
    public void testDependency() throws Exception {

        String fooJava =
        "package foo; "+
        "class Foo { "+
        "  public boolean equals(Object o) { return o instanceof Foo; } "+
        "  public int hashCode() { return 0xF00; } "+
        "}";

        String barJava =
        "package foo; "+
        "class Bar { "+
        "  Foo foo() { "+
        "    return new Foo(); "+
        "  } "+
        "}";

        SimpleCompiler compiler = new SimpleCompiler();
        Class<?> fooCls = compiler.compile("foo.Foo", fooJava);
        Class<?> barCls = compiler.compile("foo.Bar", barJava);

        SimpleInvoker fooInv = new SimpleInvoker(fooCls);
        Object foo1 = fooInv.newInstance();
        assertEquals(0xF00, foo1.hashCode());

        SimpleInvoker barInv = new SimpleInvoker(barCls);
        Object bar = barInv.newInstance();
        Object foo2 = barInv.invoke(bar, "foo");
        assertEquals(foo1, foo2);
    }

    @Test
    public void testDependencyPackages() throws Exception {

        String fooJava =
        "package foo; "+
        "public class Foo { "+
        "  public boolean equals(Object o) { return o instanceof Foo; } "+
        "  public int hashCode() { return 0xF00; } "+
        "}";

        String barJava =
        "package bar; "+
        "import foo.Foo; "+
        "public class Bar { "+
        "  Foo foo() { "+
        "    return new Foo(); "+
        "  } "+
        "}";

        SimpleCompiler compiler = new SimpleCompiler();
        Class<?> fooCls = compiler.compile("foo.Foo", fooJava);
        Class<?> barCls = compiler.compile("bar.Bar", barJava);

        SimpleInvoker fooInv = new SimpleInvoker(fooCls);
        Object foo1 = fooInv.newInstance();
        assertEquals(0xF00, foo1.hashCode());

        SimpleInvoker barInv = new SimpleInvoker(barCls);
        Object bar = barInv.newInstance();
        Object foo2 = barInv.invoke(bar, "foo");
        assertEquals(foo1, foo2);
    }

    @Test
    public void testDependencyDefaultPackage() throws Exception {

        String fooJava =
        "class Foo { "+
        "  public boolean equals(Object o) { return o instanceof Foo; } "+
        "  public int hashCode() { return 0xF00; } "+
        "}";

        String barJava =
        "class Bar { "+
        "  Foo foo() { return new Foo(); } "+
        "}";

        SimpleCompiler compiler = new SimpleCompiler();
        Class<?> fooCls = compiler.compile("Foo", fooJava);
        Class<?> barCls = compiler.compile("Bar", barJava);

        SimpleInvoker fooInv = new SimpleInvoker(fooCls);
        Object foo1 = fooInv.newInstance();
        assertEquals(0xF00, foo1.hashCode());

        SimpleInvoker barInv = new SimpleInvoker(barCls);
        Object bar = barInv.newInstance();
        Object foo2 = barInv.invoke(bar, "foo");
        assertEquals(foo1, foo2);
    }

    @Test
    public void testCyclic() throws Exception {

        String barJava =
        "package foo; "+
        "class Bar { "+
        "  class Baz {} "+
        "  Foo foo(Baz baz) { return new Foo(); } "+
        "}";

        String fooJava =
        "package foo; "+
        "class Foo { "+
        "  Bar bar() { return new Bar(); } "+
        "  Bar.Baz baz() { return bar().new Baz(); } "+
        "  public boolean equals(Object o) { return o instanceof Foo; } "+
        "  public int hashCode() { return 0xF00; } "+
        "}";

        SimpleCompiler compiler = new SimpleCompiler();
        compiler.compile(sourceFile("foo.Bar", barJava), sourceFile("foo.Foo", fooJava));

        Class<?> fooCls = compiler.loadClass("foo.Foo");
        SimpleInvoker fooInv = new SimpleInvoker(fooCls);
        Object foo1 = fooInv.newInstance();

        Object bar1 = fooInv.invoke(foo1, "bar");
        Class<?> barCls = bar1.getClass();
        assertEquals("foo.Bar", barCls.getName());

        Object baz = fooInv.invoke(foo1, "baz");
        Class<?> bazCls = baz.getClass();
        assertEquals("foo.Bar$Baz", bazCls.getName());

        SimpleInvoker barInv = new SimpleInvoker(barCls);
        Object bar2 = barInv.newInstance();
        Object foo2 = barInv.invoke(bar2, "foo", baz);
        assertEquals(foo1, foo2);
    }

    @Test
    public void testCyclicPackages() throws Exception {

        String barJava =
        "package bar; "+
        "import foo.Foo; "+
        "public class Bar { "+
        "  public class Baz {} "+
        "  Foo foo(Baz baz) { return new Foo(); } "+
        "}";

        String fooJava =
        "package foo; "+
        "import bar.Bar; "+
        "public class Foo { "+
        "  Bar bar() { return new Bar(); } "+
        "  Bar.Baz baz() { return bar().new Baz(); } "+
        "  public boolean equals(Object o) { return o instanceof Foo; } "+
        "  public int hashCode() { return 0xF00; } "+
        "}";

        SimpleCompiler compiler = new SimpleCompiler();
        compiler.compile(sourceFile("bar.Bar", barJava), sourceFile("foo.Foo", fooJava));

        Class<?> fooCls = compiler.loadClass("foo.Foo");
        SimpleInvoker fooInv = new SimpleInvoker(fooCls);
        Object foo1 = fooInv.newInstance();

        Object bar1 = fooInv.invoke(foo1, "bar");
        Class<?> barCls = bar1.getClass();
        assertEquals("bar.Bar", barCls.getName());

        Object baz = fooInv.invoke(foo1, "baz");
        Class<?> bazCls = baz.getClass();
        assertEquals("bar.Bar$Baz", bazCls.getName());

        SimpleInvoker barInv = new SimpleInvoker(barCls);
        Object bar2 = barInv.newInstance();
        Object foo2 = barInv.invoke(bar2, "foo", baz);
        assertEquals(foo1, foo2);
    }

}
