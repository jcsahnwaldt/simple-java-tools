compile:
	gradle compileJava

test:
	gradle cleanTest test

clean:
	$(RM) -r .gradle/ build/ bin/
